# README #

This is a started project that will create a workspace in which to practice typescript without other frameworks.
This can also be used as the skeleton project for a ts-node based node app.


### What is this repository for? ###

* TypeScript based node starter project
* Version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Requires nodeJS (global install)
* Requires ts-node (npm install -g ts-node)
* Requires typings (npm install -g @types/node)
* Requires tslint (npm install -g tslint)
* After installing the above, run npm install

### Wait a minute, so... how to run my code? ###

* In a command line shell type:
~~~~
    ts-node main.ts
~~~~
* Replace main.ts with whichever main script file you want to run.

### How do I reference node packages in typescript format? ###

* See this example from the main.ts file

~~~~
    // Node require statement for Filesystem would be:
    const fs = require('fs');

    // These are examples of how to import node packages in TypeScript notation (for ts-node)
    import * as fs from 'fs';
    import * as timers from 'timers';

    // These are examples of how to import your own packages
    import { ValueHistory } from './models/value-history.model';
    import { ValuePerformance } from './models/value-performance.model';
~~~~

### Deploying to a server (when running a node app as a daemon type process)

* Use pm2 (see http://pm2.keymetrics.io/ for extensive documentation)
* Remember that yours will be typescript node (ts-node)
* So if pm2 start main.ts doesn't work (besides compile errors) then see the section on transpilers
* For transpilers see: http://pm2.keymetrics.io/docs/tutorials/using-transpilers-with-pm2
* This project should be fine running pm2 start main.ts (or whatever your main script is called)

### Who do I talk to? ###

* Can contact me at conradiemj@gmail.com if you have any questions