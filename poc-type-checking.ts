/**
 * Tests type checking on union values
 */
export class A {
    x: number;
    y: number;

    constructor(xx: number, yy: number) {
        this.x = xx;
        this.y = yy;
    }
}

export class B {
    w: string;
    z: string;

    constructor(ww: string, zz: string) {
        this.w = ww;
        this.z = zz;
    }
}


function showValues(AB: A|B): void {
    if (AB instanceof A) {
        console.log(`AB: x=${(<A>AB).x}, y = ${(<A>AB).y}`);
    } else if (AB instanceof B) {
        console.log(`AB: w=${(<B>AB).w}, z = ${(<B>AB).z}`);
    }
}

let AB: A | B;

AB = new A(10,20);

showValues(AB);

AB = new B("B.W", "B.Z");

showValues(AB);