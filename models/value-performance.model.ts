/**
 * ValuePerformance stores the performance
 * of a collection of ValueHistory entries
 */
import { ValueHistory } from './value-history.model';

export enum ChangeDirection {
    DECREASED = -1,
    NO_CHANGE = 0,
    INCREASED = 1
}

export class ValuePerformance {
    public history: ValueHistory;
    public change: number;
    public performance: ChangeDirection;

    constructor(valueHistory: ValueHistory) {
        this.history = valueHistory;
        this.change = this.history.current - this.history.previous;
        this.performance = (this.change === 0) ? ChangeDirection.NO_CHANGE : (this.change > 0) ? ChangeDirection.INCREASED : ChangeDirection.DECREASED;
    }

    public static ChangeDirectonAsText(value: ChangeDirection): string {
        switch (value) {
            case ChangeDirection.DECREASED:
                return 'Decreased';
            case ChangeDirection.INCREASED:
                return 'Increase';
            case ChangeDirection.NO_CHANGE:
                return 'No change';
            default:
                return 'Unknown Change Direction';
        }
    }
}