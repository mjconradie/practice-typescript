/**
 * ValueHistory stores a pair of values, previous value and current value.
 */

export class ValueHistory {
    previous: number;
    current: number;

    constructor(current: number, previous: number) {
        this.current = current;
        this.previous = previous;
    }
}