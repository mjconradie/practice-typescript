/**
 * Calculates the performance of a ValueHistory item
 * Notes:
 * Illustrates using array functions to perform map-reduce and other
 * operations that simplify the expressions and reduces execution time.
 * References:
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some?v=example
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort?v=example
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map?v=example
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce?v=example
 */
import { ValueHistory } from './value-history.model';
import { ValuePerformance, ChangeDirection } from './value-performance.model';

export class PerformanceCalculator {
    private history: ValueHistory[] = [];
    private performance: ValuePerformance[] = [];

    constructor(valuesHistory: ValueHistory[]) {
        this.history = valuesHistory;
    }

    public setHistoricalValues(newValues: ValueHistory[]): void {
        this.history = newValues;
    }

    public addHistoricalValues(additionalValues: ValueHistory[]): void {
        // Similar to Array<T>.addRange(....) in other languages
        for (let value of additionalValues) {
            this.history.push(value);
        }
    }

    public calculatePerformance(): ValuePerformance[] {
        this.performance = [];
        this.performance = this.history.map( (vh: ValueHistory) => {
            return new ValuePerformance(vh);
        });

        // this.performance = ValuePerformance[];
        // for (let idx = 0; idx < this.history.length; idx++) {
        //     let newElement = new ValuePerformance(this.history[idx]);
        //     this.performance.push(newElement);
        // }

        return this.performance;
    }

    public overAllPerformanceAsText(): string {
        return ValuePerformance.ChangeDirectonAsText(this.overAllPerformance());
    }

    public overAllPerformance(): ChangeDirection {
        let score = this.performance.map( (valuePerformance: ValuePerformance) => {
            return valuePerformance.performance;
        }).reduce( (previousValue, currentValue) => {
            return previousValue + currentValue;
        });
        return (score === 0) ? ChangeDirection.NO_CHANGE : (score < 0) ? ChangeDirection.DECREASED : ChangeDirection.INCREASED;
        /*
        - Alternatively:
        ...............

        switch (score) {
            case -1:
                return ChangeDirection.DECREASED;
            case 0:
                return ChangeDirection.NO_CHANGE;
            case 1:
                return ChangeDirection.INCREASED;
        } */
    }

    /**
     * Finds the best performing value
     */
    bestPerformance(): ValuePerformance {
        let orderedValues = this.performance.sort( (a: ValuePerformance, b: ValuePerformance) => {
            // returning -1 places a before b, returning 1 places a after b
            // there is no purpose to returning 0 as we're looking for the best performer.
            return a.change > b.change ? -1 : 1;
        });
        return orderedValues && orderedValues.length > 0 ? orderedValues[0] : null;
    }

    /**
     * Determines if any of the values under performed, decreased in value.
     */
    anyUnderPerformers(): boolean {
        return this.performance.some( (value: ValuePerformance) => {
            return value.performance === ChangeDirection.DECREASED;
        });
    }
}