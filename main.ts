
/**
 * Main typescript practice file
 * @author "Marius Conradie"<conradiemj@gmail.com>
 * @see https://mjconradie@bitbucket.org/mjconradie/practice-typescript.git
 * Note: It's a public and open repo, clone it to get a space in which to practice typescript
 * Relies on some packages that need global install (ts-node, nodeJS and typings, see package.json)
 */
// these are examples of how to import node packages
import * as fs from 'fs';
import * as timers from 'timers';

// these are examples of how to import your own packages
import { ValueHistory } from './models/value-history.model';
import { ValuePerformance } from './models/value-performance.model';
import { PerformanceCalculator } from './models/performance-calculator.model';


let values: ValueHistory[] = [];
let performance: ValuePerformance[];
let performanceCalculator: PerformanceCalculator;

console.log('Hello World');

function generateValues(): ValueHistory[] {
    let values: ValueHistory[] = [];
    for (let idx = 0; idx < 10; idx++) {
        let current = Math.random() * 1000 - Math.random()*10*idx;
        let previous = Math.random() * 1000 - Math.random()*10*idx;
        values.push(new ValueHistory(current, previous));
    }
    return values;
}

values = generateValues();

performanceCalculator = new PerformanceCalculator(values);
performance = performanceCalculator.calculatePerformance();

console.log('Values:');
console.dir(values);
console.log('Performance:');
console.dir(performance);
console.log(performanceCalculator.bestPerformance());
console.log(`Any under performers ? ${performanceCalculator.anyUnderPerformers()}`);
console.log(`Overall performance: ${performanceCalculator.overAllPerformance()}`);
console.log(`Overall performance: ${performanceCalculator.overAllPerformanceAsText()}`);

/**
 * More examples
 */

/**
 * Trim all string value fields in a Customer object instance
 */
//   public trimStringFields( obj: any): any {
//     let cleanedObj = obj;
//     for (let property in cleanedObj) {
//       if (cleanedObj.hasOwnProperty(property)) {
//         if (typeof cleanedObj[property] === 'string') {
//           cleanedObj[property] = cleanedObj[property].trim();
//         } else if (typeof cleanedObj[property] === 'object') {
//           cleanedObj[property] = this.trimStringFields(cleanedObj[property]);
//         }
//       }
//     }
//     return cleanedObj;
//   }